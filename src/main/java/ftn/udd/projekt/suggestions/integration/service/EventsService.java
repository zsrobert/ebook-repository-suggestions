package ftn.udd.projekt.suggestions.integration.service;

import org.springframework.stereotype.Component;

import ftn.udd.projekt.suggestions.integration.event.BookViewedEvent;

@Component
public class EventsService {

	public void receiveEvent(BookViewedEvent event) {
		System.out.println("User " + event.getUser().getUserId()
				+ " viewed book " + event.getBook().getId());
	}
}

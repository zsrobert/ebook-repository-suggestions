package ftn.udd.projekt.suggestions.model;

import java.util.HashSet;
import java.util.Set;

import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedToVia;
import org.springframework.data.neo4j.support.index.IndexType;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ftn.udd.projekt.suggestions.relation.RecommendRelationship;
import ftn.udd.projekt.suggestions.relation.RelationshipType;

@NodeEntity
public class Book extends BaseModel {
	@SuppressWarnings("deprecation")
	@Indexed(unique = true, indexType = IndexType.SIMPLE)
	private String bookId;
	
	private String title;
	
	@RelatedToVia(type = RelationshipType.RECOMMENDED)
	private Set<RecommendRelationship> booksRecommendRelationships = new HashSet<RecommendRelationship>();
	
	public Book(String bookId) {
		this.bookId = bookId;
	}

	public Book() {
		// TODO Auto-generated constructor stub
	}
	
	public Book(String bookId, String title) {
		super();
		this.bookId = bookId;
		this.title = title;
	}

	@JsonIgnore
	public Set<RecommendRelationship> getBooksRecommendRelationships() {
		return booksRecommendRelationships;
	}

	public Set<Book> getAllBooksRecommendations() {

		Set<Book> recommendProducts = new HashSet<Book>();

		for (RecommendRelationship recommendRelationship : this.booksRecommendRelationships) {
			recommendProducts.add(recommendRelationship.getEnd());
		}

		return recommendProducts;
	}

	public void setBooksRecommendRelationships(
			Set<RecommendRelationship> booksRecommendRelationships) {
		this.booksRecommendRelationships = booksRecommendRelationships;
	}

	public String getBookId() {
		return bookId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bookId == null) ? 0 : bookId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Book other = (Book) obj;
		if (bookId == null) {
			if (other.bookId != null)
				return false;
		} else if (!bookId.equals(other.bookId))
			return false;
		return true;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}

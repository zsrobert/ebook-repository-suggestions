package ftn.udd.projekt.suggestions.integration.event;

import org.springframework.context.ApplicationEvent;

import ftn.udd.projekt.suggestions.dto.BookDTO;
import ftn.udd.projekt.suggestions.dto.UserDTO;
import ftn.udd.projekt.suggestions.model.Book;
import ftn.udd.projekt.suggestions.model.User;

public class BookViewedEvent extends ApplicationEvent {
	private static final long serialVersionUID = -2873980380251928055L;
	private UserDTO user;
	private BookDTO book;

	public BookViewedEvent(Object source, UserDTO user, BookDTO book) {
		super(source);
		this.user = user;
		this.book = book;
	}

	public BookViewedEvent(Object source) {
		super(source);
	}

	public UserDTO getUser() {
		return user;
	}

	public void setUser(UserDTO user) {
		this.user = user;
	}

	public BookDTO getBook() {
		return book;
	}

	public void setBook(BookDTO book) {
		this.book = book;
	}

}

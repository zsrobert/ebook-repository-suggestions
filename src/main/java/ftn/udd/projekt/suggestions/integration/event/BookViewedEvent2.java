package ftn.udd.projekt.suggestions.integration.event;

import ftn.udd.projekt.suggestions.dto.BookDTO;
import ftn.udd.projekt.suggestions.dto.UserDTO;

public class BookViewedEvent2 {
	private UserDTO user;
	private BookDTO book;

	public BookViewedEvent2(UserDTO user, BookDTO book) {
		this.user = user;
		this.book = book;
	}

	public BookViewedEvent2() {
	}

	public UserDTO getUser() {
		return user;
	}

	public void setUser(UserDTO user) {
		this.user = user;
	}

	public BookDTO getBook() {
		return book;
	}

	public void setBook(BookDTO book) {
		this.book = book;
	}

}

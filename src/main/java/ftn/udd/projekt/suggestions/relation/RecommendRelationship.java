package ftn.udd.projekt.suggestions.relation;

import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.Fetch;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.data.neo4j.annotation.StartNode;

import ftn.udd.projekt.suggestions.model.Book;

@RelationshipEntity(type = RelationshipType.RECOMMENDED)
public class RecommendRelationship {
	@GraphId
	private Long id;

	@StartNode
	@Fetch
	private Book start;

	@EndNode
	@Fetch
	private Book end;
	
	private int count = 1;

	public RecommendRelationship(Book start, Book end) {
		this.start = start;
		this.end = end;
	}
	
	public RecommendRelationship() {
		// TODO Auto-generated constructor stub
	}
	
	public Book getStart() {
		return start;
	}

	public void setStart(Book start) {
		this.start = start;
	}

	public Book getEnd() {
		return end;
	}

	public void setEnd(Book end) {
		this.end = end;
	}

	public Long getId() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((end == null) ? 0 : end.hashCode());
		result = prime * result + ((start == null) ? 0 : start.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RecommendRelationship other = (RecommendRelationship) obj;
		if (end == null) {
			if (other.end != null)
				return false;
		} else if (!end.equals(other.end))
			return false;
		if (start == null) {
			if (other.start != null)
				return false;
		} else if (!start.equals(other.start))
			return false;
		return true;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int incrementCount() {
		return ++count;
	}

}

package ftn.udd.projekt.suggestions.dto;

public class UserDTO {
	private String userId;

	public UserDTO(String userId) {
		this.userId = userId;
	}
	
	public UserDTO() {
		// TODO Auto-generated constructor stub
	}
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
}

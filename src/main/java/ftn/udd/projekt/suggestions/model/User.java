package ftn.udd.projekt.suggestions.model;

import java.util.HashSet;
import java.util.Set;

import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedToVia;
import org.springframework.data.neo4j.support.index.IndexType;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ftn.udd.projekt.suggestions.relation.ClickedRelationship;
import ftn.udd.projekt.suggestions.relation.RelationshipType;

@NodeEntity
public class User extends BaseModel {

	/**
	 * Session id.
	 */
	@SuppressWarnings("deprecation")
	@Indexed(unique = true, indexType = IndexType.SIMPLE)
	private String userId;
	
	@RelatedToVia(type = RelationshipType.CLICKED)
	private Set<ClickedRelationship> clickedBooksRelationships = new HashSet<ClickedRelationship>();

	private Book clickedBefore;

	public User(String userId) {
		this.userId = userId;
	}

	public User() {
		// TODO Auto-generated constructor stub
	}

	public Set<Book> getAllClickedBooks() {
		Set<Book> clickedProducts = new HashSet<Book>();

		for (ClickedRelationship clickedRelationship : this.clickedBooksRelationships) {
			clickedProducts.add(clickedRelationship.getBook());
		}

		return clickedProducts;
	}
	
	@JsonIgnore
	public Set<ClickedRelationship> getClickedBooksRelationships() {
		return clickedBooksRelationships;
	}

	public String getUserId() {
		return userId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		return true;
	}

	public Book getClickedBefore() {
		return clickedBefore;
	}

	public void setClickedBefore(Book clickedBefore) {
		this.clickedBefore = clickedBefore;
	}

}

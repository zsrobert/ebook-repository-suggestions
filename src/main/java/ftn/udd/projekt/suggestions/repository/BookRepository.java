package ftn.udd.projekt.suggestions.repository;

import java.util.List;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.repository.query.Param;

import ftn.udd.projekt.suggestions.model.Book;

public interface BookRepository extends GraphRepository<Book> {

	/**
	 * Returns book recommendations. Only considers immediate nodes (books).
	 * 
	 * @param bookId
	 * @return
	 */

	@Query("START book = node:Book(bookId={bookId}) MATCH book-[r:recommended]->otherBook "
			+ "RETURN otherBook ORDER BY r.count DESC LIMIT 5")
	List<Book> findOtherUsersAlsoViewedBooks(@Param("bookId") String bookId);

	/**
	 * Returns book recommendations. Only considers those immediate nodes
	 * (books) that have not been viewed by the user.
	 * 
	 * @param bookId
	 * @param userId
	 * @return
	 */

	@Query("START book = node:Book(bookId={bookId}), user = node:User(userId={userId}) "
			+ "MATCH book-[r:recommended]->otherBook "
			+ "WHERE NOT (user-[:clicked]->otherBook) "
			+ "RETURN otherBook ORDER BY r.count DESC LIMIT 5")
	List<Book> findOtherUsersAlsoViewedBooksWithoutAlreadyViewed(
			@Param("bookId") String bookId, @Param("userId") String userId);

	/**
	 * Returns book recommendations. Considers nodes (books) on a path of the
	 * provided length.
	 * 
	 * @param bookId
	 * @param pathLength
	 * @return
	 */

	@Query("START book = node:Book(bookId={bookId}) MATCH path = book-[r:recommended *..3]->otherBook "
			+ "WITH otherBook, reduce(res = 0, x in extract(x in relationships(path) | x.count) | x) as nodeCount "
			+ "RETURN otherBook ORDER BY nodeCount DESC LIMIT 5")
	List<Book> findOtherUsersAlsoViewedBooksOnPath(
			@Param("bookId") String bookId);

	/**
	 * Returns book recommendations. Considers nodes (books) on a path of the
	 * provided length that have not been viewed by the user.
	 * 
	 * @param bookId
	 * @param userId
	 * @param pathLength
	 * @return
	 */

	@Query("START book = node:Book(bookId={bookId}), user = node:User(userId={userId}) "
			+ "MATCH path = book-[r:recommended *..3]->otherBook "
			+ "WITH user, otherBook, "
			+ "reduce(res = 0, x in extract(x in relationships(path) | x.count) | x) as nodeCount "
			+ "WHERE NOT (user-[:clicked]->otherBook) "
			+ "RETURN otherBook ORDER BY nodeCount DESC LIMIT 5")
	List<Book> findOtherUsersAlsoViewedBooksWithoutAlreadyViewedOnPath(
			@Param("bookId") String bookId, @Param("userId") String userId);
}

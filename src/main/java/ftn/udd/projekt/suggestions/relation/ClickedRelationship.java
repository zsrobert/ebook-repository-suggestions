package ftn.udd.projekt.suggestions.relation;

import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.Fetch;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.data.neo4j.annotation.StartNode;

import ftn.udd.projekt.suggestions.model.Book;
import ftn.udd.projekt.suggestions.model.User;

@RelationshipEntity(type = RelationshipType.CLICKED)
public class ClickedRelationship {

	@GraphId
	private Long id;

	@StartNode
	@Fetch
	private User user;

	@EndNode
	@Fetch
	private Book book;

	private int count = 1;

	public ClickedRelationship(User user, Book book) {
		this.user = user;
		this.book = book;
	}
	
	public ClickedRelationship() {
		// TODO Auto-generated constructor stub
	}
	
	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((book == null) ? 0 : book.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClickedRelationship other = (ClickedRelationship) obj;
		if (book == null) {
			if (other.book != null)
				return false;
		} else if (!book.equals(other.book))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}

	public Long getId() {
		return id;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int incrementCount() {
		return ++count;
	}
}

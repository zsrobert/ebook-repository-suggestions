package ftn.udd.projekt.suggestions.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ftn.udd.projekt.suggestions.dto.BookDTO;
import ftn.udd.projekt.suggestions.integration.event.BookViewedEvent2;
import ftn.udd.projekt.suggestions.model.Book;
import ftn.udd.projekt.suggestions.model.User;
import ftn.udd.projekt.suggestions.repository.BookRepository;
import ftn.udd.projekt.suggestions.repository.UserRepository;
import ftn.udd.projekt.suggestions.service.RelationshipService;

@RestController
public class SuggestionsController {

	@Autowired
	private BookRepository bookRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RelationshipService relationshipService;

	@RequestMapping(value = "/suggestions/{id}", method = RequestMethod.GET)
	private List<BookDTO> handleGetSuggestions(@PathVariable String id) {
		List<BookDTO> dtos = new ArrayList<>();

		for (Book book : bookRepository.findOtherUsersAlsoViewedBooks(id)) {
			dtos.add(new BookDTO(book));
		}

		return dtos;
	}

	@RequestMapping(value = "/view", method = RequestMethod.POST)
	private ResponseEntity<byte[]> handlePostEvent(
			@RequestBody BookViewedEvent2 event, HttpServletRequest request) {
		System.out.println(event.getBook().getTitle());
		System.out.println(request.getContentType());
		try {
			
			User user = userRepository.findByPropertyValue("userId", event.getUser().getUserId());
			Book book = bookRepository.findByPropertyValue("bookId", event.getBook().getId());
			
			if (user == null) {
				user = new User(event.getUser().getUserId());
				userRepository.save(user);
			}
			
			if (book == null) {
				book = new Book(event.getBook().getId());
				bookRepository.save(book);
			}
			
			System.out.println("User " + event.getUser().getUserId()
					+ " viewed book \"" + event.getBook().getTitle()
					+ (user.getClickedBefore() != null ? "\", before that user clicked: \""
					+ user.getClickedBefore().getTitle() + "\"" : ", and hasn't clicked anything before."));
			book.setTitle(event.getBook().getTitle());
			relationshipService.addClickedBook(user, book);
			bookRepository.save(book);
			userRepository.save(user);
			
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}

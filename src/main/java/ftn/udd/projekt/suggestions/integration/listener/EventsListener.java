package ftn.udd.projekt.suggestions.integration.listener;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.stereotype.Component;

@Component
public class EventsListener implements MessageListener {
	public void onMessage(Message message) {
		String messageBody = new String(message.getBody());
		System.out.println("Listener received message----->" + messageBody);
	}
}
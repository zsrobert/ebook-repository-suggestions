package ftn.udd.projekt.suggestions.repository;

import org.springframework.data.neo4j.repository.GraphRepository;

import ftn.udd.projekt.suggestions.relation.RecommendRelationship;

public interface RecommendRelationshipRepository extends GraphRepository<RecommendRelationship> {

}

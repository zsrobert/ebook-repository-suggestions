package ftn.udd.projekt.suggestions.actor;

import akka.actor.UntypedActor;
import ftn.udd.projekt.suggestions.integration.event.BookViewedEvent;
import ftn.udd.projekt.suggestions.model.Book;
import ftn.udd.projekt.suggestions.model.User;
import ftn.udd.projekt.suggestions.repository.BookRepository;
import ftn.udd.projekt.suggestions.repository.UserRepository;
import ftn.udd.projekt.suggestions.service.RelationshipService;

public class RecordEventActor extends UntypedActor {

	private final RelationshipService relationshipService;
	private final BookRepository bookRepository;
	private final UserRepository userRepository;

	public RecordEventActor(RelationshipService relationshipService,
			BookRepository bookRepository, UserRepository userRepository) {
		this.relationshipService = relationshipService;
		this.bookRepository = bookRepository;
		this.userRepository = userRepository;
	}

	@Override
	public void onReceive(Object message) throws Exception {
		if (message instanceof BookViewedEvent) {
			BookViewedEvent event = (BookViewedEvent) message;
			User user = new User(event.getUser().getUserId());
			Book book = new Book(event.getBook().getId());
			book.setTitle(event.getBook().getTitle());
			
			//Transaction tx = graphDatabaseService.beginTx();
			try {
				relationshipService.addClickedBook(user, book);
				bookRepository.save(book);
				userRepository.save(user);
			//	tx.success();
//				System.out.println("Record - Success");
			} catch (Exception e) {
				//tx.failure();
//				System.out.println(e.getMessage());
				System.out.println("Record - Fail");
			} finally {
				//tx.close();
//				System.out.println("Record - Close");
			}
		}
	}

}

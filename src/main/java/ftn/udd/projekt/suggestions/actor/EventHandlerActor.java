package ftn.udd.projekt.suggestions.actor;

import java.util.List;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.routing.RoundRobinPool;
import ftn.udd.projekt.suggestions.integration.event.BookViewedEvent;
import ftn.udd.projekt.suggestions.model.Book;
import ftn.udd.projekt.suggestions.repository.BookRepository;
import ftn.udd.projekt.suggestions.repository.UserRepository;
import ftn.udd.projekt.suggestions.service.RelationshipService;

public class EventHandlerActor extends UntypedActor {

//	private final ActorRef fetchActor;
	private final ActorRef recordActor;

	@SuppressWarnings("unused")
	private final RelationshipService relationshipService;
	@SuppressWarnings("unused")
	private final BookRepository bookRepository;
	@SuppressWarnings("unused")
	private final UserRepository userRepository;

	public EventHandlerActor(RelationshipService relationshipService,
			BookRepository bookRepository, UserRepository userRepository) {
		this.relationshipService = relationshipService;
		this.bookRepository = bookRepository;
		this.userRepository = userRepository;

//		this.fetchActor = this.getContext().actorOf(
//				Props.create(FetchRecommendationsActor.class, bookRepository)
//						.withRouter(new RoundRobinPool(10)), "fetchActor");
		this.recordActor = this.getContext().actorOf(
				Props.create(RecordEventActor.class, relationshipService,
						bookRepository, userRepository).withRouter(
						new RoundRobinPool(10)), "recordActor");
	}

	@Override
	public void onReceive(Object message) throws Exception {
		if (message instanceof BookViewedEvent) {
			this.recordActor.tell(message, getSelf());
//			this.fetchActor.tell(((BookViewedEvent) message).getBook(),
//					getSelf());
		} else if (message instanceof List<?>) {
			@SuppressWarnings("unchecked")
			List<Book> books = (List<Book>) message;
			System.out.println(books.size());
			for (Book book : books) {
				System.out.println("Recommended: ");
				System.out.println("\t" + book.getBookId());
			}
		}
	}

}

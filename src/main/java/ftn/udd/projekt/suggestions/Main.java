package ftn.udd.projekt.suggestions;

import org.apache.log4j.xml.DOMConfigurator;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {
	public static void main(String[] args) {
		DOMConfigurator.configure("src\\main\\resources\\log4j.xml");
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
				"classpath:app-config.xml");

		Neo4jPersister neo4jPersister = (Neo4jPersister) context
				.getBean("neo4jPersister");

		neo4jPersister.createTestData();
//		neo4jPersister.runTest();

//		context.close();
	}
}

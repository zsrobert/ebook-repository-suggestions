package ftn.udd.projekt.suggestions.actor;

import akka.actor.UntypedActor;
import ftn.udd.projekt.suggestions.model.Book;
import ftn.udd.projekt.suggestions.repository.BookRepository;

public class FetchRecommendationsActor extends UntypedActor {

	private final BookRepository bookRepository;

	public FetchRecommendationsActor(BookRepository bookRepository) {
		this.bookRepository = bookRepository;
	}

	@Override
	public void onReceive(Object message) throws Exception {
		if (message instanceof Book) {
//			Transaction tx = graphDatabaseService.beginTx();
			try {
				getSender()
						.tell(bookRepository.findOtherUsersAlsoViewedBooks(((Book) message)
								.getBookId()), getSelf());
//				tx.success();
//				System.out.println("Fetch - Success");
			} catch (Exception e) {
//				tx.failure();
				System.out.println("Fetch - Fail");
			} finally {
//				tx.close();
//				System.out.println("Fetch - Close");
			}
		}
	}

}

package ftn.udd.projekt.suggestions.integration.test;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import ftn.udd.projekt.suggestions.dto.BookDTO;
import ftn.udd.projekt.suggestions.dto.UserDTO;
import ftn.udd.projekt.suggestions.integration.event.BookViewedEvent;

@Component
public class Sender implements ApplicationEventPublisherAware {
	public ApplicationEventPublisher applicationEventPublisher;
	
	public static void main(String[] args) throws Exception {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
				"classpath:sender-config.xml");
		
		Sender s = context.getBean(Sender.class);
		for (int i = 0; i < 10; i++)
		s.applicationEventPublisher.publishEvent(new BookViewedEvent(s,
				new UserDTO("pera"), new BookDTO("one")));
		context.close();
	}

	public void setApplicationEventPublisher(
			ApplicationEventPublisher applicationEventPublisher) {
		this.applicationEventPublisher = applicationEventPublisher;
	}
}

package ftn.udd.projekt.suggestions.integration.notifier;

import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import ftn.udd.projekt.suggestions.integration.event.BookViewedEvent;

@Component
public class EventNotifier implements ApplicationListener<BookViewedEvent>{

	@Override
	public void onApplicationEvent(BookViewedEvent event) {
		System.out.println("Notification: User " + event.getUser().getUserId()
				+ " viewed book " + event.getBook().getId());
	}

}

package ftn.udd.projekt.suggestions.model;

import org.springframework.data.neo4j.annotation.GraphId;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class BaseModel {
	@JsonIgnore
	@GraphId
    private Long graphId;

	public Long getGraphId() {
		return graphId;
	}
	
}

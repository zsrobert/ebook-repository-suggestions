package ftn.udd.projekt.suggestions.dto;

import ftn.udd.projekt.suggestions.model.Book;

public class BookDTO {
	private String id;
	private String title;
	
	public BookDTO() {
		// TODO Auto-generated constructor stub
	}
	
	public BookDTO(Book book) {
		this.id = book.getBookId();
		this.title = book.getTitle();
	}
	
	public BookDTO(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
}

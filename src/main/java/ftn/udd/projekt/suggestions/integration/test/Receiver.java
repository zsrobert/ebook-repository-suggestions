package ftn.udd.projekt.suggestions.integration.test;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Receiver {
	@SuppressWarnings("resource")
	public static void main(String[] args) {
		@SuppressWarnings("unused")
		ClassPathXmlApplicationContext c = new ClassPathXmlApplicationContext(
				"classpath:receiver-config.xml");
		System.out.println("STARTED");
	}

}

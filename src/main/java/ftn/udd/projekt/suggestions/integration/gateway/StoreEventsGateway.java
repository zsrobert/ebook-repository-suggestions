package ftn.udd.projekt.suggestions.integration.gateway;

import ftn.udd.projekt.suggestions.integration.event.BookViewedEvent;

public interface StoreEventsGateway {
	public void receiveEvent(BookViewedEvent event);
}

package ftn.udd.projekt.suggestions;

import java.util.List;

import org.neo4j.graphdb.GraphDatabaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.routing.RoundRobinPool;
import ftn.udd.projekt.suggestions.actor.EventHandlerActor;
import ftn.udd.projekt.suggestions.integration.event.BookViewedEvent;
import ftn.udd.projekt.suggestions.model.Book;
import ftn.udd.projekt.suggestions.model.User;
import ftn.udd.projekt.suggestions.repository.BookRepository;
import ftn.udd.projekt.suggestions.repository.UserRepository;
import ftn.udd.projekt.suggestions.service.RelationshipService;

@Component
@Transactional
public class Neo4jPersister {

	@Autowired
	private BookRepository bookRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RelationshipService relationshipService;

	private ActorRef eventHandler;

	private ActorSystem system;

	private void userClickedBook(User user, Book book) {
		// eventHandler.tell(new BookViewedEvent(this, user, book),
		// eventHandler);

		try {
			relationshipService.addClickedBook(user, book);
			bookRepository.save(book);
			userRepository.save(user);
		} catch (Exception e) {
		} finally {
		}

	}

	private Book createBook(String id) {
		return bookRepository.save(new Book(id));
	}

	private User createUser(String id) {
		return userRepository.save(new User(id));
	}

	public void createTestData() {
		this.system = ActorSystem.create("SuggestionsEngine");
		eventHandler = system.actorOf(
				Props.create(EventHandlerActor.class, relationshipService,
						bookRepository, userRepository).withRouter(
						new RoundRobinPool(10)), "eventHandler");

		User a = createUser("pera");
		User b = createUser("mika");
		User c = createUser("zika");

		Book one = createBook("1");
		one.setTitle("Xamarin Cross-platform Application Development");
		Book two = createBook("2");
		two.setTitle("Visual Studio 2013 Cookbook");
		Book three = createBook("3");
		three.setTitle("Raspberry Pi Robotic Projects");
		Book four = createBook("4");
		four.setTitle("Raspberry Pi Cookbook for Python Programmers");
		//
		userClickedBook(a, one);
		userClickedBook(a, four);
		userClickedBook(a, two);
		userClickedBook(a, three);

		userClickedBook(b, one);
		userClickedBook(b, four);
		userClickedBook(b, one);
		userClickedBook(b, four);

		userClickedBook(c, two);
		userClickedBook(c, four);
		// userClickedBook(null, new Book("one"));

		// for (Book book : bookRepository.findAll()) {
		// System.out.println(book.getBookId());
		// }

		// this.system.shutdown();
	}

	public void runTest() {
		List<Book> books = bookRepository.findOtherUsersAlsoViewedBooks("one");

		System.out.println("Recommend: ");

		for (Book book : books) {
			System.out.println("\t" + book.getBookId());
		}

		books = bookRepository
				.findOtherUsersAlsoViewedBooksWithoutAlreadyViewed("one",
						"zika");

		System.out.println("Recommend, not clicked: ");

		for (Book book : books) {
			System.out.println("\t" + book.getBookId());
		}

		books = bookRepository.findOtherUsersAlsoViewedBooksOnPath("one");

		System.out.println("Recommend; on path: ");

		for (Book book : books) {
			System.out.println("\t" + book.getBookId());
		}

		books = bookRepository
				.findOtherUsersAlsoViewedBooksWithoutAlreadyViewedOnPath("one",
						"zika");

		System.out.println("Recommend, not clicked; on path: ");

		for (Book book : books) {
			System.out.println("\t" + book.getBookId());
		}
	}

}

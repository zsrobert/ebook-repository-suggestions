package ftn.udd.projekt.suggestions.repository;

import org.springframework.data.neo4j.repository.GraphRepository;

import ftn.udd.projekt.suggestions.model.User;

public interface UserRepository extends GraphRepository<User> {

}

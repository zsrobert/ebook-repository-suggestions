package ftn.udd.projekt.suggestions.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ftn.udd.projekt.suggestions.model.Book;
import ftn.udd.projekt.suggestions.model.User;
import ftn.udd.projekt.suggestions.relation.ClickedRelationship;
import ftn.udd.projekt.suggestions.relation.RecommendRelationship;
import ftn.udd.projekt.suggestions.repository.ClickedRelationshipRepository;
import ftn.udd.projekt.suggestions.repository.RecommendRelationshipRepository;

@Component
@Transactional
public class RelationshipService {

	@Autowired
	private RecommendRelationshipRepository recommendRelationshipRepository;

	@Autowired
	private ClickedRelationshipRepository clickedRelationshipRepository;
	
	/**
	 * Automatically persist the relationship if updated.
	 * 
	 * @param book
	 * @param bookRecommend
	 */
	public void addBookRecommend(Book book, Book bookRecommend) {
		RecommendRelationship recommendRelationship = new RecommendRelationship(
				book, bookRecommend);

		if (book.getBooksRecommendRelationships().contains(
				recommendRelationship)) {
			for (RecommendRelationship relation : book
					.getBooksRecommendRelationships()) {
				if (relation.getEnd().equals(bookRecommend)) {
					relation.incrementCount();
					recommendRelationshipRepository.save(relation);
					break;
				}
			}
		} else {
			book.getBooksRecommendRelationships().add(recommendRelationship);
		}
	}
	
	/**
	 * Automatically persist the relationship if updated.
	 * 
	 * @param user
	 * @param clicked
	 */
	public void addClickedBook(User user, Book clicked) {
		ClickedRelationship clickedRelationship = new ClickedRelationship(user,
				clicked);

		if (user.getClickedBefore() != null) {
			addBookRecommend(user.getClickedBefore(), clicked);
		}

		if (user.getClickedBooksRelationships().contains(clickedRelationship)) {
			for (ClickedRelationship relation : user
					.getClickedBooksRelationships()) {
				if (relation.getBook().equals(clicked)) {
					relation.incrementCount();
					clickedRelationshipRepository.save(relation);
					break;
				}
			}
		} else {
			user.getClickedBooksRelationships().add(clickedRelationship);
		}

		user.setClickedBefore(clicked);

	}
}
